<?php

namespace Drupal\connectorg_microsites\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Microsite entities.
 *
 * @ingroup connectorg_microsites
 */
class MicrositeEntityDeleteForm extends ContentEntityDeleteForm {


}
