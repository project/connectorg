<?php

namespace Drupal\connectorg_microsites;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Microsite entities.
 *
 * @ingroup connectorg_microsites
 */
class MicrositeEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Microsite ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\connectorg_microsites\Entity\MicrositeEntity $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.microsite_entity.edit_form',
      ['microsite_entity' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
