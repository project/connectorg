<?php

namespace Drupal\connectorg_events\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Event entities.
 *
 * @ingroup connectorg_events
 */
class EventEntityDeleteForm extends ContentEntityDeleteForm {


}
