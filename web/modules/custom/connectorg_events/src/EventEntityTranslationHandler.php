<?php

namespace Drupal\connectorg_events;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for event_entity.
 */
class EventEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
