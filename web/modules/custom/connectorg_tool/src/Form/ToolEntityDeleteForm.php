<?php

namespace Drupal\connectorg_tool\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Tool entities.
 *
 * @ingroup connectorg_tool
 */
class ToolEntityDeleteForm extends ContentEntityDeleteForm {


}
