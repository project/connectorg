<?php

namespace Drupal\connectorg_news\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting News entities.
 *
 * @ingroup connectorg_news
 */
class NewsEntityDeleteForm extends ContentEntityDeleteForm {


}
