<?php

namespace Drupal\connectorg_news;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for news_entity.
 */
class NewsEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
