<?php

namespace Drupal\connectorg_birthdays\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Birthday entities.
 */
class BirthdayEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
