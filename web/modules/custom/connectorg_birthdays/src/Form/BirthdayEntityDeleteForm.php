<?php

namespace Drupal\connectorg_birthdays\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Birthday entities.
 * @ingroup connectorg_birthdays
 */
class BirthdayEntityDeleteForm extends ContentEntityDeleteForm
{
}
