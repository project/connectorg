<?php

namespace Drupal\connectorg_hero;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for hero_entity.
 */
class HeroEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
