<?php

namespace Drupal\connectorg_hero\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Hero entity entities.
 *
 * @ingroup connectorg_hero
 */
class HeroEntityDeleteForm extends ContentEntityDeleteForm {


}
