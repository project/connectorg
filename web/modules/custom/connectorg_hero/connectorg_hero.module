<?php

/**
 * @file
 * Contains connectorg_hero.module.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function connectorg_hero_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the connectorg_hero module.
    case 'help.page.connectorg_hero':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Module to handle Hero information') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function connectorg_hero_theme($existing, $type, $theme, $path) {
  return [
    'hero_entity' => [
      'path' => $path . '/templates',
      'template' => 'hero-entity',
      'render element' => 'elements',
    ],
  ];
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function connectorg_hero_theme_suggestions_hero_entity(array $variables) {
  $suggestions = [];
  $sanitized_view_mode = strtr($variables['elements']['#view_mode'], '.', '_');
  $suggestions[] = 'hero_entity__' . $sanitized_view_mode;
  return $suggestions;
}

/**
 * Implements hook_preprocess_HOOK().
 */
function connectorg_hero_preprocess_hero_entity(&$variables) {
  $variables['content'] = [];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
  $entity = $variables['elements']['#hero_entity'];
  $variables['url'] = $entity->toUrl()->toString();
}
