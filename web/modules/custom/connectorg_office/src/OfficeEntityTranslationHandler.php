<?php

namespace Drupal\connectorg_office;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for office_entity.
 */
class OfficeEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
